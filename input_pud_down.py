import RPi.GPIO as GP
import time

cnl = 7
# PIN 7 AND 3.3V
# normally 0 when connected 1

GP.setmode(GP.BOARD)
GP.setup(cnl, GP.IN,GP.PUD_DOWN)
while(True):
	print(GP.input(cnl))
	time.sleep(1)
