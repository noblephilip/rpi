import RPi.GPIO as GPIO
import time

led1 = 8
led2 = 10

GPIO.setmode(GPIO.BOARD)
GPIO.setup(led1, GPIO.OUT, initial = 0)
GPIO.setup(led2, GPIO.OUT, initial = 1)
try:
	while(True):
		time.sleep(2)
		GPIO.output(led1,1)
		GPIO.output(led2,0)
		time.sleep(1)
		GPIO.output(led1,GPIO.LOW)
		GPIO.output(led2,GPIO.HIGH)
except KeyboardInterrupt:
	GPIO.cleanup()
	print("Exiting..") 

