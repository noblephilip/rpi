#!/usr/bin/python3
import RPi.GPIO as GPIO
import time
led = 8
ledstate = True
def switchledstate(channel):
	global ledstate
	global led
	print("Event Occured")
	ledstate = not(ledstate)
	GPIO.output(led,ledstate)

swtch = 7
GPIO.setmode(GPIO.BOARD)
GPIO.setup(swtch,GPIO.IN, GPIO.PUD_DOWN)
GPIO.setup(led,GPIO.OUT,initial=ledstate)
GPIO.add_event_detect(swtch, GPIO.BOTH,switchledstate,600)
try:
	
	while(True):
		#to avoid 100% CPU usage
		time.sleep(1)
		print('...')
except KeyboardInterrupt:
	#cleanup GPIO settings before exiting
	GPIO.cleanup()
	print("Exiting")

